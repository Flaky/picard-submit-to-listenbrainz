# ListenBrainz Submission Plugin for Picard

A plugin for MusicBrainz Picard, for importing listens into ListenBrainz.

## Rationale

This plugin is intended for those who manually have to import their listens into ListenBrainz for any reason. Such use cases include:
- using a media player or streaming that doesn't support, or isn't supported by ListenBrainz.
- listening to music on a physical medium (e.g. listening to a CD on a CD player or listening to a vinyl record)

Anyone who has moved to ListenBrainz from Last.fm may have heard of Open (Web) Scrobbler, a manual scrobbler utility. MusicBee users may also be aware of ScrobbleItem, a plugin for MusicBee which does a similar thing. This is essentially the ListenBrainz equivalent for those.

## Installation
Download the source code as a ZIP archive and drop it into your Picard plugins folder. To access it, go to your plugin settings in Picard and click "Open plugin folder".

**Do not rename the .zip file.** Picard will have trouble opening it if you do.

## OS Compatibility
I have tested this on Fedora 37 on the Linux side, and Windows 11 on the Windows side. Both are confirmed working just fine.

Unfortunately, I do not have a Mac, therefore I cannot test whether this will work on macOS.  It should work as the plugin communicates to ListenBrainz through Qt's networking modules which Picard does have a dependency on itself.

## Limitations
As of right now, the only limitations right now are to do with how releases with no timestamps are handled when submitting releases. See issue #11 and the to-do for more details.

I have also held off on adding ISRCs to the submission data as it's uncertain how ISRCs should be handled in this use case, particularly when MBIDs are already submitted and there can be multiple ISRCs to one recording.

## To-do

- [x] Submit release listens
  - [x] Allow inputting supported services on Digital Media releases.
  - [ ] Allow inputting lengths in MM:SS to alleviate the lack of a duration on some recordings. 
- [x] Test this plugin on Windows.
- [x] Submit standalone recording listens.
- [x] Submit listens where the user has just finished listening.
- [X] Stop relying on Python's request module, ~~using Picard's equivalent instead~~.
  - QtNetwork is used instead, as it supports tweaking the Authorization header to authenticate ListenBrainz users.
- Catch/raise more exceptions and handle them in the code.
