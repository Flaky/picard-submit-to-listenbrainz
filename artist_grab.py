from picard.track import Track
artist_tags = ["display artist", "DISPLAY ARTIST", "artist"]

'''
This is intended for a reusable artist tag grabber function. Not all artist tags are in the artist field.

If none of the artist tags return anything, it is assumed the artist tag is straight-up missing, so return [unknown] (MusicBrainz's standard artist parameter for unknown artists)

ARTIST TAGS:
"display artist": if this is here, the person using Picard uses MusicBee, which if configured with split/multi-value artist tags under the artist tag, uses a "display artist" when viewing artist info or submitting to Last.fm.

"artist": the default
'''

def grab_artist_tag(track):
    tag_missing = True
    if isinstance(track, Track):
        for tag in artist_tags:
            if track.metadata[tag]:
                tag_missing = False
                return track.metadata[tag]
                break
        if tag_missing:
            return "[unknown]"
    else:
        raise TypeError("Not a track.")
