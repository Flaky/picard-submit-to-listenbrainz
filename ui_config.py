from PyQt5.QtWidgets import (
    QVBoxLayout,
    QHBoxLayout,
    QGroupBox,
    QLabel,
    QLineEdit
    )


class SubmitToListenBrainzOptionsUI():

    def __init__(self, page):
        super().__init__()
        self.main_container = QVBoxLayout()
        self.desc_groupbox = QGroupBox()
        self.desc_groupbox.setTitle("Description")
        self.desc_groupbox_layout = QVBoxLayout()

        self.description = QLabel()
        self.description.setText("This plugin allows you to import past listens to ListenBrainz. This is useful if you are using a media player or streaming service that doesn't support/isn't supported by ListenBrainz, or are submitting listens correspondent to a physical medium (e.g. listening to a CD on a CD player or listening to a vinyl record).")
        self.desc_groupbox_layout.addWidget(self.description)
        self.desc_groupbox.setLayout(self.desc_groupbox_layout)
        self.main_container.addWidget(self.desc_groupbox)

        self.userauth_groupbox = QGroupBox()
        self.userauth_groupbox.setTitle("Authentication")
        self.userauth_groupbox_layout = QVBoxLayout()
        self.userauth_id_layout = QHBoxLayout()
        self.userauth_desc = QLabel()
        self.userauth_desc.setText("In order to submit listens to ListenBrainz, a valid user token is required. To grab this, go to https://listenbrainz.org/profile/ and copy your token into the textbox.")
        self.userauth_groupbox_layout.addWidget(self.userauth_desc)
        self.userauth_textbox = QLineEdit()
        self.userauth_textbox.setPlaceholderText("ListenBrainz ID")
        self.userauth_id_layout.addWidget(self.userauth_textbox)
        self.userauth_groupbox_layout.addLayout(self.userauth_id_layout)
        self.userauth_groupbox.setLayout(self.userauth_groupbox_layout)
        self.main_container.addWidget(self.userauth_groupbox)

        self.main_container.addStretch()
        page.setLayout(self.main_container)
